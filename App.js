import 'react-native-gesture-handler';
import { enableScreens } from 'react-native-screens';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import AppNavigator from './src/root/navigation'


const App = () => {


  return (
    <NavigationContainer>
       <AppNavigator/>
    </NavigationContainer>
  );
};



export default App;
