import React from 'react';

import {
  View,
  Text,
  SafeAreaView,
  Image,
  StatusBar,
  TouchableNativeFeedback,
  TouchableOpacity,
} from 'react-native';
import {Styles} from '../utils/Styles';
import Icon from 'react-native-vector-icons/AntDesign';
import color from '../utils/Color';
import {useNavigation} from '@react-navigation/native';

const Headerscomponent = ({name,hide}) => {
  const Navigation = useNavigation();
  console.warn(name)
  return (
    <SafeAreaView style={{backgroundColor: color.white}}>
      {hide == true && 
            <TouchableOpacity onPress={()=>Navigation.goBack()}>
            <Icon
                    name="arrowright"
                    type="AntDesign"
                    color={color.black}
                    style={{marginHorizontal: 10,alignSelf:'flex-end',marginTop:'2%'}}
                    size={25}
                  />
            </TouchableOpacity>
      }


    </SafeAreaView>
  );
};

export default Headerscomponent;
