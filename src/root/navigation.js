import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import Downlod from 'react-native-vector-icons/Feather';

import Pod from 'react-native-vector-icons/MaterialCommunityIcons';

import color from '../utils/Color';
const Stack = createStackNavigator();
const Tabs = createBottomTabNavigator();
import Firstscreen from '../screen/Firstscreen';
import Secendscreen from '../screen/Secendscreen';
import Thirthscreen from '../screen/Thirthscreen';
import Fourthscreen from '../screen/Fourthscreen';
import Fifth from '../screen/Fifth';

import Headerscomponent from './Headers';
const library = () => {
  return (
    <Stack.Navigator
    screenOptions={{
      header: (props) => <Headerscomponent {...props} />,
    }}
    >
      <Stack.Screen component={Firstscreen} name="Firstscreen"/>
      <Stack.Screen component={Fifth} name="Fifth" />
    </Stack.Navigator>
  );
};
const podcast = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen component={Secendscreen} name="Secendscreen" />
    </Stack.Navigator>
  );
};
const bookshell = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen component={Thirthscreen} name="Thirthscreen" 
            options={{
              header: () => (
                <Headerscomponent name='romel' hide={true}/>
              ),
            }}
      />
    </Stack.Navigator>
  );
};
const user = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen component={Fourthscreen} name="Fourthscreen" />
    </Stack.Navigator>
  );
};


const AppNavigator = () => {
  return (
    <Tabs.Navigator
          tabBarOptions={{
        keyboardHidesTabBar: true,
        labelStyle: {fontSize: 15},
        activeTintColor: color.orange,
        style: {height: 50},
        showLabel: true,
      }}
      initialRouteName='Thirthscreen'
    >
      <Tabs.Screen
        component={library}
        name="Firstscreen"
        options={{
          title: 'کتابخانه',
          tabBarIcon: ({color}) => (
            <Downlod name="download" size={25} color={color} />
          ),
        }}
      />
      <Tabs.Screen
        component={podcast}
        name="Secendscreen"
        options={{
          title: 'پاد کست',
          tabBarIcon: ({color}) => (
            <Pod name="google-podcast" size={25} color={color} />
          ),
        }}
      />
      <Tabs.Screen
        component={bookshell}
        name="Thirthscreen"
        options={{
          title: 'کتاب صوتی',
          tabBarIcon: ({color}) => (
            <Pod name="bookshelf" size={25} color={color} />
          ),
        }}
      />
      <Tabs.Screen
        component={user}
        name="Fourthscreen"
        options={{
          title: 'پروفایل',
          tabBarIcon: ({color}) => (
            <Downlod name="user" size={25} color={color} />
          ),
        }}
      />
    </Tabs.Navigator>
  );
};

export default AppNavigator;
