import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  Dimensions,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {Styles, thirthscreen} from '../utils/Styles';
import color from '../utils/Color';
import {soundbook,flatlistdata} from '../server/StaticData';
import CustombtnComponent from '../Component/BtnComponent/CustombtnComponent';
import {Divider} from '../Component/divider';
import Arrow from 'react-native-vector-icons/MaterialIcons';
import RenderComponent from '../Component/RenderComponent/renderComponent';
const Thirthscreen = () => {
  return (
    <View style={Styles.container}>
      <ScrollView>
        <View style={thirthscreen.backimage}>
          <Image
            source={require('../asset/image/pic.jpg')}
            style={Styles.image}
          />
          <TouchableOpacity style={thirthscreen.playbottom}>
            <Image
              source={require('../asset/image/play.png')}
              style={Styles.image}
            />
          </TouchableOpacity>
        </View>

        <View style={thirthscreen.textview}>
          <Text style={[Styles.title, Styles.margintop]}>مسافر کوچولو</Text>
          <Text style={[Styles.text, Styles.margintop2]}>
            آنتوان دوسنت اگزوپری
          </Text>
        </View>
        <View style={thirthscreen.mapview}>
          {soundbook.map(e => {
            return (
              <View
                style={[
                  thirthscreen.dataview,
                  Styles.margintop,
                  {
                    borderRightWidth: e.id === 3 ? 0 : 1,
                    width: e.id === 2 ? '39%' : '30%',
                  },
                ]}>
                    <View style={{justifyContent:'center',alignItems:'center'}}>
                    <Image source={e.image} />
                <Text
                  style={[
                    Styles.text,
                    Styles.margintop2,
                    {fontSize: e.id === 1 ? 16 : 12},
                  ]}>
                  {e.title}
                </Text>
                    </View>
 
              </View>
            );
          })}
        </View>
        <CustombtnComponent
          title="دریافت"
          textStyle={{color: color.white, fontSize: 18}}
          bottomStyle={Styles.margintop}
        />
        <View
          style={[
            thirthscreen.textview,
            Styles.margintop,
            {width: '90%', backgroundColor: color.lightgray},
          ]}>
          <Text style={[Styles.text, Styles.margintop2, thirthscreen.textbox]}>
            تاب صوتی مسافر کوچولو اثر آنتوان دو سنت اگزوپری روایت‌گر زندگی
            شازده‌ کوچولویی است که در یکی از این سیاره‌های خیلی کوچک به تنهایی
            زندگی می‌کند او از تنهایی خسته و افسرده شده بود و آرزو داشت کسی بود
            تا با اون حرف می‌زد و از تنهایی در می‌آمد. یک روز با تعجب دید که در
            سیاره‌ی کوچکش یک گیاه درآمده و…
          </Text>
          <View style={thirthscreen.circle}>
            <Arrow name="keyboard-arrow-down" size={25} color={color.gray} />
          </View>
          <Divider lineColor="transparent" marginTop={2} />
        </View>
        <View
          style={[
            Styles.view,
            Styles.margintop,
            Styles.padding,
            Styles.rowEnd,
          ]}>
          <Text style={[Styles.text, {color: color.black, fontSize: 16}]}>
            پیشنهادها
          </Text>
          <Divider lineColor="transparent" marginTop={2} />
          <FlatList
            data={flatlistdata}
            horizontal={true}
            renderItem={({item}) => <RenderComponent item={item} />}
            keyExtractor={item => item.id}
          />
        </View>
        <Divider lineColor="transparent" />
      </ScrollView>
    </View>
  );
};

export default Thirthscreen;
