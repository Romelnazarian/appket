import React, {useState, useRef} from 'react';
import {
  View,
  Text,
  ImageBackground,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {Styles} from '../utils/Styles';
import Pluse from 'react-native-vector-icons/AntDesign';
import color from '../utils/Color';
import {styles} from 'ansi-colors';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

const textdata = [
  {
    id: 1,
    title: 'شماره کارت بانکی برای احراز شماست',
    text: 'نوعی وابسته به متن می‌باشد آنها با استفاده از محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی و صفحه‌بندی را به پایان برن',
  },
  {
    id: 2,
    title: 'شماره کارت بانکی برای احراز شماست',
    text: 'نوعی وابسته به متن می‌باشد آنها با استفاده از محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی و صفحه‌بندی را به پایان برن',
  },
  {
    id: 3,
    title: 'شماره کارت بانکی برای احراز شماست',
    text: 'نوعی وابسته به متن می‌باشد آنها با استفاده از محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی و صفحه‌بندی را به پایان برن',
  },
];

const Firstscreen = ({navigation}) => {
  const [firstValue, setfirstValue] = useState('');
  const [SecendValue, setSecendValue] = useState('');
  const [ThirdValue, setThirdValue] = useState('');
  const [ForthValue, setFourthValue] = useState('');

  const [Var, setVar] = useState('');

  const ref0 = useRef();
  const ref1 = useRef();
  const ref2 = useRef();
  const ref3 = useRef();

  const btn = () => {
    const options = {
      enableVibrateFallback: true,
      ignoreAndroidSystemSettings: true,
    };
    ReactNativeHapticFeedback.trigger('impactMedium', options);
  };
  return (
    <ImageBackground
      source={require('../asset/image/backimage.jpeg')}
      style={Styles.container}>
      <View style={{width: '65%', alignSelf: 'center', marginTop: '10%'}}>
        <Text style={Styles.title}>کارت های بانکی</Text>
        <Text style={Styles.title}>خود را وارد کنید</Text>
      </View>
      <View
        style={{
          width: '90%',
          alignSelf: 'center',
          marginTop: '10%',
          height: 140,
          borderWidth: 1,
          borderColor: 'white',
          borderRadius: 10,
          backgroundColor: 'rgba(255,255,255,0.3)',
          //   position: 'relative',
        }}>
        <TouchableOpacity>
          <Pluse
            name="pluscircleo"
            size={25}
            style={{alignSelf: 'center', marginTop: '2%'}}
            color={color.white}
          />
        </TouchableOpacity>
        <Text
          style={[
            Styles.text,
            {
              position: 'absolute',
              right: '15%',
              top: '6%',
              color: 'rgba(255,255,255,0.3)',
            },
          ]}>
          bank name
        </Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'center',
            marginTop: '8%',
          }}>
          <TextInput
            maxLength={4}
            ref={ref0}
            placeholder="----"
            placeholderTextColor={color.white}
            style={{
              width: '20%',
              textAlign: 'center',
              color: color.white,
            }}
            autoCorrect={true}
            keyboardType="numeric"
            // returnKeyType="done"
            // autoFocus={true}
            value={firstValue}
            onChangeText={val => setfirstValue(val)}
            onChange={() => {
              {
                Var == 1 ? '' : firstValue.length == 3 && ref1.current.focus();
              }
            }}
            onKeyPress={event => {
              if (event.nativeEvent.key == 'Backspace') {
                if (firstValue.length == 1) {
                  setVar('');
                  return;
                } else {
                  setSecendValue(firstValue.length - 1);
                }
              } else {
                alert('Something else Pressed'); // show a valid alert with the key info
              }
            }}
          />
          <TextInput
            maxLength={4}
            ref={ref1}
            placeholder="----"
            placeholderTextColor={color.white}
            style={{
              width: '20%',
              textAlign: 'center',
              color: color.white,
            }}
            autoCorrect={true}
            keyboardType="numeric"
            // returnKeyType="done"
            // autoFocus={true}
            value={SecendValue}
            onChangeText={val => setSecendValue(val)}
            onChange={() => {
              {
                Var == 1 ? '' : SecendValue.length == 3 && ref2.current.focus();
              }
            }}
            onKeyPress={event => {
              if (event.nativeEvent.key == 'Backspace') {
                if (SecendValue.length == 1) {
                  setVar('1');
                  ref0.current.focus();
                } else {
                  setSecendValue(SecendValue.length - 1);
                }
              } else {
                alert('Something else Pressed'); // show a valid alert with the key info
              }
            }}
          />
          <TextInput
            returnKeyType="next"
            ref={ref2}
            placeholder="----"
            placeholderTextColor={color.white}
            style={{
              width: '20%',
              textAlign: 'center',
              color: color.white,
            }}
            onChangeText={val => setThirdValue(val)}
            onChange={() => {
              {
                Var == 1 ? '' : ThirdValue.length == 3 && ref3.current.focus();
              }
            }}
            keyboardType="numeric"
            onKeyPress={event => {
              if (event.nativeEvent.key == 'Backspace') {
                //   console.warn(SecendValue.length)
                if (ThirdValue.length == 1) {
                  setVar('');
                  ref1.current.focus();
                } else {
                  setThirdValue(ThirdValue.length - 1);
                }
              } else {
                alert('Something else Pressed'); // show a valid alert with the key info
              }
            }}
          />
          <TextInput
            value={ForthValue}
            maxLength={4}
            ref={ref3}
            placeholder="----"
            placeholderTextColor={color.white}
            style={{
              width: '20%',
              textAlign: 'center',
              color: color.white,
            }}
            keyboardType="numeric"
            onChangeText={val => setFourthValue(val)}
            onKeyPress={event => {
              if (event.nativeEvent.key == 'Backspace') {
                //   console.warn(SecendValue.length)
                if (ForthValue.length == 1) {
                  setVar('1');
                  ref2.current.focus();
                } else {
                  setFourthValue(ForthValue.length - 1);
                }
              } else {
                alert('Something else Pressed'); // show a valid alert with the key info
              }
            }}
          />
        </View>
      </View>
      {textdata.map(e => {
        return (
          <View style={{width: '90%', alignSelf: 'center'}}>
            <Text
              style={[
                styles.text,
                {color: color.white, marginTop: '5%', fontWeight: 'bold'},
              ]}>{e.title}</Text>
            <Text
              style={[
                styles.text,
                {color: color.white, marginTop: '3%'},
              ]}>{e.text}</Text>
          </View>
        );
      })}

      <TouchableOpacity
        onPress={() => navigation.push('Fifth')}
        style={{
          width: '40%',
          height: 50,
          borderRadius: 10,
          backgroundColor: color.white,
          justifyContent: 'center',
          alignItems: 'center',
          alignSelf: 'center',
          position: 'absolute',
          bottom: 20,
        }}>
        <Text style={[styles.text, {color: 'red', fontWeight: 'bold'}]}>
          اضافه کردن
        </Text>
      </TouchableOpacity>
    </ImageBackground>
  );
};

export default Firstscreen;
