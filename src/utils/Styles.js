import fonts from './fonts';
import color from './Color';
import { Dimensions } from 'react-native';
const Height = Dimensions.get('screen').height

export const Styles = {
  //-----------------GLOBAL-----------------//
  container: {
    flex: 1,
    backgroundColor: color.white,
  },
  title: {
    ...fonts.fontFamilyBold_Android,
    fontSize: 22,
    color: color.black,
    fontWeight: 'bold',
  },
  margintop: {
    marginTop: '5%',
  },
  view:{
   width:'90%',
   alignSelf:'center'
  },
  margintop2: {
    marginTop: '2%',
  },
  text: {
    ...fonts.fontFamilyMedium_Android,
    fontSize: 14,
    color: color.gray,
  },
  image:{
    width:'100%',
    height:'100%'
  },

  btncomponent:{
    width:'50%',backgroundColor:color.purple,height:50,borderRadius:5,alignSelf:'center',justifyContent:'center',alignItems:'center'
  },
  padding:{
   paddiRight:10
  },
  rowEnd:{
    justifyContent: 'flex-end',
  },
  rowSpaceReverse: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
  },
  rowStartReverse: {
    flexDirection: 'row-reverse',
    justifyContent: 'flex-start',
  },
  headerTitle: {
    ...fonts.fontFamilyMedium_Android,
    fontSize: 14,
    color: color.black,
    textAlign: 'center',
    alignSelf: 'center',
    flex: 10,
    // fontWeight:'800'
  },
  headerContainer: {
    justifyContent: 'center',
    paddingHorizontal: '2%',
    backgroundColor: color.white,
    height: '60@ms',
    elevation: 3,
  },

};
  //-----------------thirdscreen-----------------//

export const thirthscreen ={
    backimage:{
    width: '65%',
    height: Height / 2.5,
    alignSelf: 'center',
    marginTop: '10%',
  },
  playbottom:{
    position:'absolute',alignSelf:'center',top:'40%',width:50,height:50
  },
  textview:{
    width:'65%',alignSelf:'center',justifyContent:'center',alignItems:'center'
  },
  mapview:{
    width:'95%',flexDirection:'row',justifyContent:'space-between',alignSelf:'center'
  },
  dataview:{
    width:'33%',alignItems:'center',borderRightColor:color.lightgray,alignSelf:'center'
  },

  textbox:{
    width:'88%',alignSelf:'center',
    color:color.black,
  },
  circle:{
    bottom:-10,position:'absolute',width:25,height:25,borderRadius:50,borderWidth:1,borderColor:color.gray,justifyContent:'center',alignItems:'center'
  }
}