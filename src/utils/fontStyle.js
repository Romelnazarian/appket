// Common styles
import {StyleSheet, Dimensions} from 'react-native';
import fonts from './fonts';
const HEIGHT = Dimensions.get('window').height;
const WIDTH = Dimensions.get('window').width;

export default (fontStyles = StyleSheet.create({

  text: {
    ...Platform.select({
      ios: {
        fontFamily: fonts.fontFamily_Ios,
      },
      android: {
        fontFamily: fonts.fontFamilyMedium_Android,
      },
    }),
  },
  textBold: {
    ...Platform.select({
      ios: {
        fontFamily: fonts.fontFamily_Ios,
        fontWeight: 'bold',
      },
      android: {
        fontFamily: fonts.fontFamilyBold_Android,
      },
    }),
  },
  
}));
