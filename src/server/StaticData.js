import React from 'react';
import {Image} from 'react-native';

export const soundbook = [
  {id: 1, title: '۱۷:۰۲', image: require('../asset/image/time.png')},
  {
    id: 2,
    title: 'عادله نهاوندیان',
    image: require('../asset/image/narrator.png'),
  },
  {
    id: 3,
    title: 'علی محمد پور',
    image: require('../asset/image/translator.png'),
  },
];


export const flatlistdata = [
  {id: 1, image: require('../asset/image/image1.png')},
  {
    id: 2,
    image: require('../asset/image/image2.png'),
  },
  {
    id: 3,
    image: require('../asset/image/image3.png'),
  },
  {id: 4, image: require('../asset/image/image1.png')},
  {
    id: 4,
    image: require('../asset/image/image2.png'),
  },
  {
    id: 6,
    image: require('../asset/image/image3.png'),
  },
]