import React,{useState} from 'react';
import { View,Text } from 'react-native';
import {Styles} from '../../utils/Styles';
 const CustombtnComponent = (props) =>{
    return(
        <View style={[Styles.btncomponent,{...props.bottomStyle}]}>
            <Text style={[Styles.text,{...props.textStyle}]}>{props.title}</Text>
        </View>
    )
}


export default CustombtnComponent