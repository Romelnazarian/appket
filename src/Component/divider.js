import React from 'react';
import {Text, View, Dimensions, StyleSheet} from 'react-native';

import color from '../utils/Color';
import styles from '../utils/Styles';
var DEVICE_WIDTH = Dimensions.get('window').width;

export const DeepDivider = (props) => {
  return (
    <View style={[styles.columnStart, {width: '100%'}]}>
      <View
        style={[
          styles.divider,
          {backgroundColor: 'black', height: 1, width: '100%'},
        ]}
      />
      <View
        style={[
          styles.divider,
          {
            backgroundColor: 'rgba(225,225,225,0.1)',
            marginTop: -10,
            width: '100%',
            height: 0.3,
          },
        ]}
      />
    </View>
  );
};
export const Divider = ({
  lineColor,
  marginVertical,
  width,
  marginTop,
  marginRight,
  height,
  containerStyle,
}) => {
  return (
    <View
      style={[
        containerStyle,
        {
          height: height || 1,
          width: width || '100%',
          backgroundColor: lineColor || '#F0F0F0',
          marginVertical: marginVertical || 10,
          // marginTop:marginTop || 10,
          alignSelf: 'center',
          marginRight: marginRight || 0,
          marginTop:marginTop
        },
      ]}></View>
  );
};
export const VerticalDivider = ({
  height,
  lineColor,
  marginHorizontal,
  width,
}) => {
  return (
    <View
      style={{
        width: width || 1,
        height: height || '80%',
        backgroundColor: lineColor || color.gray,
        marginHorizontal: marginHorizontal || 5,
        alignSelf: 'center',
      }}></View>
  );
};
