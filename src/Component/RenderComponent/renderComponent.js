import React,{useState} from 'react';
import { View,Text,Image} from 'react-native';

import {Styles} from '../../utils/Styles'
 const RenderComponent = ({item}) =>{
    return(
        <View style={Styles.margin2}>
            <Image source={item.image}/>
        </View>
    )
}


export default RenderComponent;